package video;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;
import video.bean.VideoInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/28 20:09
 * @version 1.0
 */
public class TencentVideo {

    private String URL_VIDEO_TENCENT = "https://v.qq.com/x/search/?q=";

    public List<VideoInfo> query(String keyword) {
        List<VideoInfo> ret = new ArrayList<>();

        //图片路径
        List<String> imgUrlList = new ArrayList();
        List<String> titleList = new ArrayList();
        List<String> pageUrlList = new ArrayList();

        Spider.create(new PageProcessor() {

            @Override
            public void process(Page page) {
                Html html = page.getHtml();
                Selectable imgUrl = html.$("img.figure_pic", "src");
                imgUrlList.addAll(imgUrl.all());

                Selectable title = html.$("img.figure_pic", "alt");
                titleList.addAll(title.all());


                Selectable pageUrl = html.$("a.figure.result_figure", "href");
                pageUrlList.addAll(pageUrl.all());
            }

            @Override
            public Site getSite() {
                return Site.me();
            }
        }).addUrl(URL_VIDEO_TENCENT + keyword).run();


        System.out.println(imgUrlList.size());
        System.out.println(titleList.size());
        System.out.println(pageUrlList.size());

        for (int i = 0; i < imgUrlList.size(); i++) {
            VideoInfo vi=new VideoInfo();
            vi.setImgUrl(imgUrlList.get(i));
            vi.setTitle(titleList.get(i));
            vi.setPageUrl(pageUrlList.get(i));
            ret.add(vi);
        }

        return  ret;
    }

}
