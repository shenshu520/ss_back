package com.ss_back.config;

import com.wheel.custom.exception.BizException;
import com.wheel.custom.exception.SysException;
import com.wheel.custom.result.Result;
import com.wheel.custom.result.ResultStateE;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/26 16:13
 * @version 1.0
 */
@ControllerAdvice
public class ExceptionHandlerConfig {

    @ResponseBody
    @ExceptionHandler(value =Exception.class)
    public Result exceptionHandler(Exception e){
        System.out.println("未知异常！原因是:"+e);
        return Result.ERR;
    }

    @ResponseBody
    @ExceptionHandler(value = BizException.class)
    public Result bizExceptionHandler(BizException e){
        return new Result(e.getMsg(),null,e.getCode(), ResultStateE.FAIL);
    }


    @ResponseBody
    @ExceptionHandler(value = SysException.class)
    public Result sysExceptionHandler(SysException e){
        return new Result(e.getMsg(),null,e.getCode(), ResultStateE.ERR);
    }
}
