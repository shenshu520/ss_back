package com.ss_back;

import com.wheel.boot.component.RedisComponent;
import com.wheel.boot.config.RedisConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @description: 启动器
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/13 14:45
 * @version 1.0
 */
@SpringBootApplication
@MapperScan("com.ss_back.module.**.mapper")
@ComponentScan(basePackageClasses = RedisComponent.class)
@ComponentScan(basePackages = "com.ss_back")
public class Application implements ApplicationRunner {


    @Value("${projectContextName}")
    public String projectContextName;


    public static void main(String[] args) {
        // Spring应用启动起来
        SpringApplication.run(Application.class,args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("项目配置环境:"+projectContextName);
    }

}
