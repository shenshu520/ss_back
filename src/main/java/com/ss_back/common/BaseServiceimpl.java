package com.ss_back.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wheel.boot.common.WheelBootServiceImpl;
import com.wheel.boot.common.entity.IdIntegerEntity;
import org.springframework.stereotype.Service;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/16 12:09
 * @version 1.0
 */
public class BaseServiceimpl<M extends BaseMapper<T>, T extends IdIntegerEntity> extends WheelBootServiceImpl<M,T,Integer> implements  IBaseService<T> {


}
