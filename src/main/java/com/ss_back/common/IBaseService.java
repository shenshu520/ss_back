package com.ss_back.common;

import com.wheel.boot.common.IWheelBootService;
import com.wheel.boot.common.entity.IdIntegerEntity;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/16 12:09
 * @version 1.0
 */
public interface IBaseService<T extends IdIntegerEntity> extends IWheelBootService<T,Integer> {


}
