package com.ss_back.common;

import com.wheel.boot.common.controller.WheelBootApiController;
import com.wheel.boot.common.controller.WheelBootBaseController;
import com.wheel.boot.common.entity.IdIntegerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/15 11:23
 * @version 1.0
 */
@Component
public class BaseController extends WheelBootBaseController<Integer> {


    public static final String FORMAT_TOKEN = "token_%s";

    @Autowired
    RedisTemplate<String, Object> redis;

    @Override
    public Integer getSessionUserId() {
        String token = getRequest().getHeader("token");
        String key = String.format(FORMAT_TOKEN, token);
        Object userId = redis.opsForValue().get(key);
        if (userId == null) {
            return null;
        }
        return Integer.valueOf(userId.toString());
    }

}
