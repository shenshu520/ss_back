package com.ss_back.common.consts;

/**
 * @description: 返回CODE
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/26 16:39
 * @version 1.0
 */
public enum ResultCodeEnum {

    CODE_1000("没有发现会话","1010");

    private String msg;
    private String code;

    ResultCodeEnum(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
