package com.ss_back.common;

import com.wheel.boot.common.controller.WheelBootApiController;
import com.wheel.boot.common.entity.IdIntegerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/15 11:23
 * @version 1.0
 */
public class BaseApiController<T extends IdIntegerEntity> extends WheelBootApiController<T,Integer> {


    @Autowired
    BaseController baseController;

    @Override
    public Integer getSessionUserId() {
        return baseController.getSessionUserId();
    }

}
