package com.ss_back.module.sys.user.service.impl;

import com.ss_back.module.sys.user.entity.SysUser;
import com.ss_back.module.sys.user.mapper.SysUserMapper;
import com.ss_back.module.sys.user.service.ISysUserService;
import com.ss_back.common.BaseServiceimpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
@Service
public class SysUserServiceImpl extends BaseServiceimpl<SysUserMapper, SysUser> implements ISysUserService {

}
