package com.ss_back.module.sys.user.controller;


import com.ss_back.module.sys.user.entity.SysUser;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.ss_back.common.BaseApiController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseApiController<SysUser> {

}
