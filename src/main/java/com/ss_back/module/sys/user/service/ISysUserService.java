package com.ss_back.module.sys.user.service;

import com.ss_back.module.sys.user.entity.SysUser;
import com.ss_back.common.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
public interface ISysUserService extends IBaseService<SysUser> {

}
