package com.ss_back.module.sys.user.mapper;

import com.ss_back.module.sys.user.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
