package com.ss_back.module.sys.user.entity;

import java.time.LocalDateTime;
import com.wheel.boot.common.entity.IdIntegerEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
@ApiModel(value="SysUser对象", description="")
public class SysUser extends IdIntegerEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账户")
    private String acc;

    @ApiModelProperty(value = "密码")
    private String pwd;



    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


    @Override
    public String toString() {
        return "SysUser{" +
            "acc=" + acc +
            ", pwd=" + pwd +
        "}";
    }
}
