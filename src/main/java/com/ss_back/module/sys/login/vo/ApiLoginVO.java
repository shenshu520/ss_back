package com.ss_back.module.sys.login.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @description: TODO
 * @author shenshu
 * @E-mail 254602848@qq.com
 * @date 2021/9/26 17:11
 * @version 1.0
 */
@ApiModel(value = "API登入")
public class ApiLoginVO {

    @ApiModelProperty(value = "账户")
    private String userName;

    @ApiModelProperty(value = "密码")
    private String password;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
