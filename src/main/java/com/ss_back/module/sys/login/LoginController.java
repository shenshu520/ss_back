package com.ss_back.module.sys.login;


import cn.hutool.core.util.CreditCodeUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ss_back.common.BaseApiController;
import com.ss_back.common.BaseController;
import com.ss_back.common.consts.ResultCodeEnum;
import com.ss_back.module.sys.login.vo.ApiLoginVO;
import com.ss_back.module.sys.user.entity.SysUser;
import com.ss_back.module.sys.user.service.ISysUserService;
import com.wheel.custom.result.Result;
import com.wheel.utils.check_data.CheckDataUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
@RestController
@RequestMapping("/sys/login")
@Api(tags = "登入")
public class LoginController extends BaseController {


    @Autowired
    ISysUserService sysUserService;


    @ResponseBody
    @RequestMapping(value = {"/apiLogin"},method = {RequestMethod.POST})
    @ApiOperation(value = "API登入", notes = "")
    public Result apiLogin(@RequestBody ApiLoginVO vo){
        String userName = vo.getUserName();
        String password = vo.getPassword();

        CheckDataUtil.checkBizEmpty(userName,"登入失败！账户不能为空");
        CheckDataUtil.checkBizEmpty(password,"登入失败！密码不能为空");

        //MD5加密
        String pwdMd5 = toPwdMd5(password);

        QueryWrapper<SysUser> qw = sysUserService.newQueryWrapper();
        qw.eq("acc",userName);
        qw.eq("pwd",pwdMd5);
        SysUser one = sysUserService.getOne(qw);
        if(one==null){
            CheckDataUtil.checkBizNull(one,"账户或者密码错误！");
        }



        return Result.SUCC;
    }





    /**
     * @description: MD5盐
     */
    private static  final String MD5_SALT="80d4329c636d";

    /**
     * @description:    密码MD5加密
     * @author shenshu
     * @date: 2021/9/26 17:41
     * @param pwd
     * @return: java.lang.String
     */
    public String toPwdMd5(String pwd){
        for (int i=0;i<3;i++){
            pwd=SecureUtil.md5(pwd+MD5_SALT);
        }
        return  pwd;
    }


}
