package com.ss_back.module.sys.session;


import com.ss_back.common.BaseController;
import com.ss_back.common.consts.ResultCodeEnum;
import com.wheel.custom.exception.BizException;
import com.wheel.custom.result.Result;
import com.wheel.utils.check_data.CheckDataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2021-09-23
 */
@RestController
@RequestMapping("/sys/session")
public class SessionController extends BaseController{

    @Autowired
    RedisTemplate<String,Object> redis;

    @ResponseBody
    @RequestMapping(value = {"/nowSessionUserId"},method = {RequestMethod.POST})
    public Result nowSessionUserId(){
        Integer sessionUserId = super.getSessionUserId();
        ResultCodeEnum code1000=ResultCodeEnum.CODE_1000;
        CheckDataUtil.checkBizNull(sessionUserId,new BizException(code1000.getMsg(),code1000.getCode()));
        return Result.newSucc(sessionUserId);
    }


    @ResponseBody
    @RequestMapping(value = {"/nowSessionRemove"},method = {RequestMethod.POST})
    public Result nowSessionRemove(){
        String token = getRequest().getHeader("token");
        String key = String.format(FORMAT_TOKEN, token);
        redis.delete(key);
        return  Result.SUCC_REMOVE;
    }




}
