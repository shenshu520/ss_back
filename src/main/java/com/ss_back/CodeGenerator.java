package com.ss_back;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.ss_back.common.BaseApiController;
import com.ss_back.common.BaseServiceimpl;
import com.ss_back.common.IBaseService;
import com.wheel.boot.common.entity.IdIntegerEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CodeGenerator {
	

	
	//数据库地址
	private static final String URL="jdbc:mysql://localhost:3306/ss_back?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC";
	
	//数据库驱动类路径
	private static final String DRIVER_NAME="com.mysql.cj.jdbc.Driver";
	
	//数据库登入账户
	private static final String USER_NAME="root";
	
	//数据库登入密码
	private static final String PASSWORD="123456";
	
	
	//项目根包路径
	private static final String ROOT_PAGE_PATH="com.ss_back.module";


	
	
	//继承的基本控制器类
	private static final Class<BaseApiController> SUPER_CONTROLLER_CLASS= BaseApiController.class;
	
	//继承的基本Service类
	@SuppressWarnings("rawtypes")
	private static final Class<IBaseService> SUPER_SERVICE_CLASS = IBaseService.class;

	//继承的基本ServiceImp类
	@SuppressWarnings("rawtypes")
	private static final Class<BaseServiceimpl> SUPER_SERVICEIMPL_CLASS = BaseServiceimpl.class;
	
	
	
	@SuppressWarnings("rawtypes")
	private static final Class<IdIntegerEntity> SUPER_ENTITY_CLASS = IdIntegerEntity.class;
	
	
	
	/**
	 * <p>
	 * 读取控制台内容
	 * </p>
	 */
	@SuppressWarnings("resource")
	public static String scanner(String tip) {
		Scanner scanner = new Scanner(System.in);
		StringBuilder help = new StringBuilder();
		help.append("请输入" + tip + "：");
		System.out.println(help.toString());
		if (scanner.hasNext()) {
			String ipt = scanner.next();
			if (StringUtils.isNotBlank(ipt)) {
				return ipt;
			}
		}
		throw new MybatisPlusException("请输入正确的" + tip + "！");
	}

	public static void main(String[] args) {
		// 代码生成器
		AutoGenerator mpg = new AutoGenerator();

		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		String projectPath = System.getProperty("user.dir");
		gc.setOutputDir(projectPath + "/src/main/java");
		gc.setAuthor("jobob");
		gc.setOpen(false);
		gc.setSwagger2(true); //实体属性 Swagger2 注解
		mpg.setGlobalConfig(gc);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setUrl(URL);
		// dsc.setSchemaName("public");
		dsc.setDriverName(DRIVER_NAME);
		dsc.setUsername(USER_NAME);
		dsc.setPassword(PASSWORD);
		mpg.setDataSource(dsc);

		// 包配置
		PackageConfig pc = new PackageConfig();
		pc.setModuleName(scanner("模块名"));
		pc.setParent(ROOT_PAGE_PATH);
		mpg.setPackageInfo(pc);

		// 自定义配置
		InjectionConfig cfg = new InjectionConfig() {
			@Override
			public void initMap() {
				// to do nothing
			}
		};

		// 如果模板引擎是 freemarker
		String templatePath = "/templates/mapper.xml.ftl";
		// 如果模板引擎是 velocity
		// String templatePath = "/templates/mapper.xml.vm";

		// 自定义输出配置
		List<FileOutConfig> focList = new ArrayList<>();
		// 自定义配置会被优先输出
		focList.add(new FileOutConfig(templatePath) {
			@Override
			public String outputFile(TableInfo tableInfo) {
				// 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
				return projectPath + "/src/main/resources/mapper/" + pc.getModuleName() + "/"
						+ tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
			}

		});
		/*
		 * cfg.setFileCreate(new IFileCreate() {
		 * 
		 * @Override public boolean isCreate(ConfigBuilder configBuilder, FileType
		 * fileType, String filePath) { // 判断自定义文件夹是否需要创建
		 * checkDir("调用默认方法创建的目录，自定义目录用"); if (fileType == FileType.MAPPER) { // 已经生成
		 * mapper 文件判断存在，不想重新生成返回 false return !new File(filePath).exists(); } //
		 * 允许生成模板文件 return true; } });
		 */
		cfg.setFileOutConfigList(focList);
		mpg.setCfg(cfg);

		// 配置模板
		TemplateConfig templateConfig = new TemplateConfig();

		// 配置自定义输出模板
		// 指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
		// templateConfig.setEntity("templates/entity2.java");
		// templateConfig.setService();
		// templateConfig.setController();

		templateConfig.setXml(null);
		mpg.setTemplate(templateConfig);

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		strategy.setNaming(NamingStrategy.underline_to_camel);
		strategy.setColumnNaming(NamingStrategy.underline_to_camel);
		strategy.setSuperEntityClass(SUPER_ENTITY_CLASS);
//		strategy.setEntityLombokModel(true);
		strategy.setRestControllerStyle(true);

		// 公共父类
		strategy.setSuperControllerClass(SUPER_CONTROLLER_CLASS);
		strategy.setSuperServiceClass(SUPER_SERVICE_CLASS);
		strategy.setSuperServiceImplClass(SUPER_SERVICEIMPL_CLASS);
		// 写于父类中的公共字段
		strategy.setSuperEntityColumns("id");
		strategy.setSuperEntityColumns("creator_user_id");
		strategy.setSuperEntityColumns("creator_date");
		strategy.setSuperEntityColumns("modified_user_id");
		strategy.setSuperEntityColumns("modified_date");



		strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
		strategy.setControllerMappingHyphenStyle(true);
		strategy.setTablePrefix(pc.getModuleName() + "_");
		mpg.setStrategy(strategy);
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
		mpg.execute();
	}
}
